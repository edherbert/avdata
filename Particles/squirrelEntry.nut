function checkboxCallback(widget, action){
    if(action == 3){ //Mouse release.
        ::enableGuiEffect(widget.getUserId(), widget.getValue());
    }
}

function createGui(){
    ::containerWin <- _gui.createWindow();
    containerWin.setSize(500, 500);
    local layout = _gui.createLayoutLine();

    local labels = [
        "Fountain",
        "Rain",
        "Aureola",
        "Swarm",
        "Snow"
    ];
    foreach(c,i in labels){
        local checkbox = ::containerWin.createCheckbox();
        checkbox.attachListener(checkboxCallback);
        checkbox.setText(i);
        checkbox.setUserId(c);

        layout.addCell(checkbox);
        if(c == 0){
            checkbox.setValue(true);
        }
    }
    layout.layout();
}

function start(){
    _doFile("res://../common/scripts/fpsCamera.nut");

    _camera.setPosition(0, 0, 15);
    //Get it looking the right way.
    _camera.lookAt(0, 0, 0);
    _camera.setPosition(0, 50, 200);

    ::fpsCamera.start(_camera.getDirection());

    createGui();

    local particleSystems = [
        "Examples/PurpleFountain",
        "Examples/Rain",
        "Examples/Aureola",
        "Examples/Swarm",
        "Examples/Snow"
    ];
    ::particleNodes <- [];
    local r = _scene.getRootSceneNode();
    foreach(c,i in particleSystems){
        local node = r.createChildSceneNode();
        ::particleSystem <- _scene.createParticleSystem(i);
        node.attachObject(particleSystem);
        node.setVisible(false);

        ::particleNodes.append(node);
    }

    enableGuiEffect(0, true);
}

::enableGuiEffect <- function(id, enabled){
    ::particleNodes[id].setVisible(enabled);
}

function update(){
    ::fpsCamera.update();
}

function end(){

}
