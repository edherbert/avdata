function start(){

    _doFile(_settings.getDataDirectory() + "/Dungeon/dungeonGen.nut");

    ::dungeonTiles <- [];
    ::restartKeyDown <- false;

    ::AcceptHandle <- _input.getButtonActionHandle("Accept");

    ::gen <- dungeonGen.generator(50, 50);
    gen.generate();

    setupDungeon(gen);
}

function setupDungeon(dungeonGenerator){
    dungeonTiles.clear();

    local windowWidth = 600;
    local windowHeight = 600;

    local squareWidth = windowWidth / dungeonGenerator.width;
    local squareHeight = windowHeight / dungeonGenerator.height;

    for(local y = 0; y < dungeonGenerator.height; y++){
        for(local x = 0; x < dungeonGenerator.width; x++){
            local d = dungeonGenerator.data[x + y * dungeonGenerator.width]
            if(d != false){
                local tex = MovableTexture("font.png");
                tex.setPosition(x * squareWidth, y * squareHeight);
                tex.setSize(squareWidth, squareHeight);

                tex.setSectionScale(8.0 / 256.0, 0.5, (d * 8.0) / 256.0, 0.0);

                dungeonTiles.append(tex);
            }
        }
    }
}

function update(){
    if(_input.getButtonAction(::AcceptHandle)){
        if(!restartKeyDown){
            restartKeyDown = true;

            gen.generate();
            setupDungeon(gen);
        }
    }else restartKeyDown = false;
}

function end(){

}
