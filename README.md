Simple data files to test the function of the avEngine.

These files aren't necessarily intended to create a full game, but rather to show the functionality of specific parts of the engine.
They act as ways to test and show functionality during development.