enum WorkspaceTypes{
    Default,
    MultiLight,
    PSSM,
    End
};

function widgetCallback(widget, action){
    if(action != 2) return;

    local idx = widget.getUserId();
    setWorkspaceDemo(idx)
}

function createWorkspaces(){
    local workspaceNames = [
        "default_finalWorkspace",
        "multiLight_finalWorkspace"
        "pssm_finalWorkspace"
    ];
    for(local i = 0; i < WorkspaceTypes.End; i++){
        local renderTexture = _window.getRenderTexture();
        local defaultWorkspace = _compositor.addWorkspace([renderTexture], _camera.getCamera(), workspaceNames[i], false);

        ::workspaces[i] <- [defaultWorkspace];
    }
}

::setWorkspaceDemo <- function(id){
    print("Switching workspaces");
    foreach(i,c in ::workspaces){
        foreach(y in c){
            y.setEnabled(i == id);
        }
    }

    local data = ::workspaces[id];
    foreach(i in data){
        i.setEnabled(true);
    }
}

function setupGui(){
    ::win <- _gui.createWindow();
    ::win.setPosition(0, 0);
    ::win.setSize(500, 500);

    local layout = _gui.createLayoutLine();
    local labels = ["Default", "Multi light", "PSSM"];
    foreach(i,c in labels){
        local button = ::win.createButton();
        button.setText(c);
        button.attachListener(widgetCallback);
        button.setUserId(i);
        layout.addCell(button);
    }
    layout.layout();
}

function setupScene(){
    local rootNode = _scene.getRootSceneNode();
    ::floorNode <- rootNode.createChildSceneNode();

    const SCENE_SIZE = 50;

    ::nodes <- [];
    local centreOffset = Vec3(SCENE_SIZE / 2, 0, SCENE_SIZE / 2);
    for(local i = 0; i < 100; i++){
        local meshNode = rootNode.createChildSceneNode();
        local meshObject = _scene.createItem("cube");
        meshNode.attachObject(meshObject);
        ::nodes.append(meshNode);
        local pos = _random.randVec3() * Vec3(SCENE_SIZE, 4, SCENE_SIZE);
        meshNode.setPosition(pos - centreOffset);
    }

    const SPOT_LIGHTS = 2;
    for(local i = 0; i < SPOT_LIGHTS; i++){
        local idx = (i.tofloat() / SPOT_LIGHTS) * PI*2;
        local pos = Vec3(sin(idx), 10, cos(idx)) * Vec3(4, 1, 4);

        local lightNode = rootNode.createChildSceneNode();
        //local lightObject = _scene.createItem("cube");
        local lightObject = _scene.createLight();

        lightObject.setType(_LIGHT_SPOTLIGHT);
        lightObject.setDiffuseColour( 0.8, 0.4, 0.2 );
        lightObject.setAttenuationBasedOnRadius( 10.0, 0.01 );
        lightObject.setSpecularColour( 0.8, 0.4, 0.2 );
        lightObject.setPowerScale(PI*2);
        lightNode.attachObject(lightObject);

        local lightDir = pos.normalisedCopy();
        //lightDir *= Vec3(-1, -1, -1);
        lightDir.y = -0.5;
        lightObject.setDirection(lightDir);


        lightNode.setPosition(pos);
    }

    local floor = _scene.createItem("cube");
    ::floorNode.attachObject(floor);
    ::floorNode.setPosition(0, -5, 0);
    ::floorNode.setScale(SCENE_SIZE, 0.1, SCENE_SIZE);
}

function start(){
    ::count <- 0;
    ::workspaces <- {};

    _doFile("res://../common/scripts/fpsCamera.nut");

    _camera.setPosition(0, 0, 20);
    _camera.lookAt(0, 0, 0);

    ::fpsCamera.start(_camera.getDirection());

    createWorkspaces();

    setWorkspaceDemo(0);

    setupScene();

    local winSize = Vec2(_window.getWidth(), _window.getHeight());
    _gui.setCanvasSize(winSize, winSize);

    setupGui();
}

function update(){
    ::fpsCamera.update();

    count += 0.01;
    if(count >= 1) count = 0;
    foreach(i in nodes){
        i.setOrientation(Quat(count, 0, 0, 1));
    }
}
