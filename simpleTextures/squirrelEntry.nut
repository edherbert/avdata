function start(){

    ::anim <- 0.0;
    ::animInt <- 0;

    ::scaleTex <- MovableTexture("cat1.jpg");

    ::movTex <- MovableTexture("cat1.jpg");

    ::sectionTex <- MovableTexture("cat1.jpg");

    ::changeTex <- MovableTexture("cat1.jpg");
    ::changeImage1 <- true; //Whether we should display image one or two.
    ::flipLayers <- true; //Keeps track of which layers switched.

    ::overlap1 <- MovableTexture("cat1.jpg");
    ::overlap2 <- MovableTexture("cat2.jpg");

    overlap1.setLayer(0);
    overlap2.setLayer(1);
    overlap1.setSize(100, 100);
    overlap2.setSize(100, 100);
    overlap1.setPosition(50, 50);
    overlap2.setPosition(100, 100);

    setTexturePositions();

    ::prevWindowWidth <- _window.getWidth();
    ::prevWindowHeight <- _window.getHeight();

}

function setTexturePositions(){
    local winWidth = _window.getWidth();
    local winHeight = _window.getHeight();

    local baseHeightSize = 0.1 * winHeight;

    local baseWidthSize = 0.1 * winWidth;
    ::baseHeightPos <- (0.5 * winHeight) - (baseHeightSize / 2);

    ::scaleTex.setPosition(0.1 * winWidth, baseHeightPos);
    ::changeTex.setPosition(0.4 * winWidth, baseHeightPos);
    ::sectionTex.setPosition(0.8 * winWidth, baseHeightPos);

    ::scaleTex.setSize(baseWidthSize, baseHeightSize);
    ::movTex.setSize(baseWidthSize, baseHeightSize);
    ::changeTex.setSize(baseWidthSize, baseHeightSize);
    ::sectionTex.setSize(baseWidthSize, baseHeightSize);
}

function update(){
    anim += 0.01;
    animInt++;

    local currWindowWidth = _window.getWidth();
    local currWindowHeight = _window.getHeight();

    if(currWindowWidth != prevWindowWidth || currWindowHeight != prevWindowHeight){
        setTexturePositions();

        prevWindowWidth = currWindowWidth;
        prevWindowHeight = currWindowHeight;
    }

    local sizeAmount = 0.1 * currWindowWidth;
    scaleTex.setWidth(sin(anim) * sizeAmount + (sizeAmount * 2));

    movTex.setPosition(0.6 * currWindowWidth, baseHeightPos + (sin(anim) * sizeAmount));

    sectionTex.setSectionScale(0.5, 0.5, (sin(anim) * 0.25) + 0.25, 0.3);

    if(animInt % 20 == 0){
        changeImage1 = !changeImage1;

        changeTex.setTexture(changeImage1 ? "cat1.jpg" : "cat2.jpg");



        flipLayers = !flipLayers;

        overlap1.setLayer(flipLayers ? 0 : 1);
        overlap2.setLayer(!flipLayers ? 0 : 1);
    }

}

function end(){
    print("ending");

}
