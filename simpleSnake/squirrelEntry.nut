function start(){

    _doFile(_settings.getDataDirectory() + "/../common/jankyGui/jankyText.nut");

    ::anim <- 0;

    ::length <- 1;

    ::squareSize <- 40;
    ::arenaWidth <- 15;
    ::arenaHeight <- 15;
    ::foodX <- 0;
    ::foodY <- 0;

    //0 - down
    //1 - left
    //2 - right
    //3 - up
    ::snakeDirection <- 0;
    ::snakeX <- 2;
    ::snakeY <- 2;

    ::snakeParts <- [];

    ::foodTex <- MovableTexture("cat1.jpg");
    foodTex.setSize(squareSize, squareSize);

    ::moveInputHandle <- _input.getAxisActionHandle("LeftMove"); //Stick
    ::moveUpHandle <- _input.getButtonActionHandle("DirectionUp"); //Dpad buttons
    ::moveDownHandle <- _input.getButtonActionHandle("DirectionDown");
    ::moveLeftHandle <- _input.getButtonActionHandle("DirectionLeft");
    ::moveRightHandle <- _input.getButtonActionHandle("DirectionRight");


    ::scoreText <- janky.text("Score: 0");
    scoreText.setPosition(10, 10);
    scoreText.setSize(16);
    scoreText.setLayer(1);


    restartGame();

}

function restartGame(){
    snakeParts.clear();
    snakeX = 2;
    snakeY = 2;

    length = 1;
    snakeDirection = 0;

    for(local i = 0; i < 2; i++){
        local snakeTex = MovableTexture("cat1.jpg");
        snakeTex.setSize(squareSize, squareSize);

        local posX = snakeX;
        local posY = snakeY - i;
        snakeTex.setPosition(posX * squareSize, posY * squareSize);

        snakeParts.push([posX, posY, snakeTex]);
    }

    repositionFood();
}

function repositionFood(){
    foodX = ((rand().tofloat() / RAND_MAX) * arenaWidth).tointeger();
    foodY = ((rand().tofloat() / RAND_MAX) * arenaHeight).tointeger();

    foodTex.setPosition(foodX * squareSize, foodY * squareSize);
}

function moveSnek(){
    if(snakeDirection == 0) snakeY++;
    if(snakeDirection == 1) snakeX--;
    if(snakeDirection == 2) snakeX++;
    if(snakeDirection == 3) snakeY--;

    if(snakeY >= arenaHeight) snakeY = 0;
    if(snakeX >= arenaWidth) snakeX = 0;
    if(snakeX < 0) snakeX = arenaWidth;
    if(snakeY < 0) snakeY = arenaHeight;

    //Check for gameover.
    foreach(i in snakeParts){
        if(i[0] == snakeX && i[1] == snakeY){
            gameOver();
        }
    }

    //TODO don't create a texture each time. Try and recycle them.
    local snakeTex = MovableTexture("cat1.jpg");
    snakeTex.setSize(squareSize, squareSize);

    snakeParts.remove(0);
    snakeTex.setPosition(snakeX * squareSize, snakeY * squareSize);
    snakeParts.push([snakeX, snakeY, snakeTex]);


    if(snakeX == foodX && snakeY == foodY){
        length++;
        repositionFood();

        scoreText.setText("Score: " + (length - 1));

        local newTex = MovableTexture("cat1.jpg");
        newTex.setSize(squareSize, squareSize);
        newTex.setPosition(snakeX * squareSize, snakeY * squareSize);

        snakeParts.push([snakeX, snakeY, newTex]);
    }
}

function gameOver(){
    print("Game Over");

    restartGame();
}

function update(){
    anim++;

    //0 - down
    //1 - left
    //2 - right
    //3 - up
    //Check the input each frame so nothing is missed.
    local xVal = _input.getAxisActionX(::moveInputHandle);
    local yVal = _input.getAxisActionY(::moveInputHandle);

    local intendedDirection = -1;
    if(xVal < -0.2) intendedDirection = 1;
    if(xVal > 0.2) intendedDirection = 2;
    if(yVal < -0.2) intendedDirection = 3;
    if(yVal > 0.2) intendedDirection = 0;

    //The dpad has higher presidence.
    if(_input.getButtonAction(::moveUpHandle)) intendedDirection = 3;
    if(_input.getButtonAction(::moveDownHandle)) intendedDirection = 0;
    if(_input.getButtonAction(::moveLeftHandle)) intendedDirection = 1;
    if(_input.getButtonAction(::moveRightHandle)) intendedDirection = 2;

    if(intendedDirection >= 0){
        if(intendedDirection == 3 && snakeDirection != 0) snakeDirection = 3;
        if(intendedDirection == 0 && snakeDirection != 3) snakeDirection = 0;
        if(intendedDirection == 1 && snakeDirection != 2) snakeDirection = 1;
        if(intendedDirection == 2 && snakeDirection != 1) snakeDirection = 2;
    }

    if(anim % 10 != 0) return;

    moveSnek();

}

function end(){

}
