::CharacterController <- {

    "__orientateAngleVec": Vec3(0, 1, 0),

    "__targetOrientation": Quat(),
    "__startOrientation": Quat(),

    function init(){
        _input.setActionSets({
            "default" : {
                "Buttons" : {
                    "Jump": "#Jump",
                    "Talk": "#Talk"
                },
                "StickPadGyro" : {
                    "Move":"#Move"
                }
            }
        });

        this.JumpHandle <- _input.getButtonActionHandle("Jump");
        this.TalkHandle <- _input.getButtonActionHandle("Talk");
        this.MoveHandle <- _input.getAxisActionHandle("Move");

        _input.mapControllerInput(0, this.MoveHandle);
        _input.mapControllerInput(0, this.JumpHandle);
        _input.mapKeyboardInput(104, this.TalkHandle);

        _input.mapKeyboardInput(122, this.JumpHandle);
        _input.mapKeyboardInputAxis(100, 115, 97, 119, this.MoveHandle);

        //Create player entity
        local e = _entity.create(SlotPosition());
        if(!e.valid()) return;
        _component.mesh.add(e, "cube");

        this.__player <- e;

        this.playerSpeed <- Vec3(0.8, 0.8, 0.8);

        setPlayerPosition(SlotPosition());
    }

    function enoughMovement(val){
        return val > 0.1 || val < -0.1;
    }

    function update(){
        local amount = Vec3(_input.getAxisActionX(this.MoveHandle), 0, _input.getAxisActionY(this.MoveHandle));
        if( enoughMovement(amount.x) || enoughMovement(amount.z) ){
            local orientation = Quat(atan2(amount.x, amount.z), __orientateAngleVec);
            movePlayer(amount * this.playerSpeed, orientation);
        }

        local newOrientation = __startOrientation.nlerp(0.5, __targetOrientation, true);
        __setPlayerOrientation(newOrientation);
    }

    function movePlayer(mov, orien){
        local playerPos = _world.getPlayerPosition();
        playerPos.move(mov);
        setPlayerPosition(playerPos);
        orientatePlayer(orien);
    }

    function setPlayerPosition(pos){
        _world.setPlayerPosition(pos);

        __player.setPosition(pos);

        _camera.setPosition(pos + SlotPosition(0, 0, 0, 20, 40));
        _camera.lookAt(pos);

        _audio.setListenerPosition(pos);
    }

    function orientatePlayer(quat){
        __startOrientation = _component.mesh.getMesh(__player).getOrientation();

        __targetOrientation = quat;

        __setPlayerOrientation(quat);
    }

    function __setPlayerOrientation(orientation){
        //__player.setOrientation(orientation);
        local mesh = _component.mesh.getMesh(__player);
        mesh.setOrientation(orientation);


        _audio.setListenerOrientation(orientation);
    }

};