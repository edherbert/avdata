function start(){
    _doFile("res://CharacterController.nut");

    ::soundBuffer <- _audio.newSoundBuffer("res://../common/audio/runningWater.wav");
    ::testSound <- _audio.newSourceFromBuffer(soundBuffer);

    _slotManager.setCurrentMap("characterControllerMap");
    _world.createWorld();

    ::CharacterController.init();

    setupAudioSources();

    setupGui();
}

function update(){
    ::CharacterController.update();
}

function end(){
    print("ending");
}

function setupGui(){
    ::win <- _gui.createWindow();
    ::win.setPosition(0, 0);
    ::win.setSize(500, 500);

    local layout = _gui.createLayoutLine();
    local labels = ["Play sound"];
    local callbacks = [
        function(widget, action){
            ::testSound.play();
        },
    ];
    foreach(i,c in labels){
        local button = ::win.createButton();
        button.setText(c);
        button.attachListenerForEvent(callbacks[i], 2);
        button.setUserId(i);
        layout.addCell(button);
    }
    layout.layout();
}

function setupAudioSources(){
    ::audioSources <- [];
    ::audioMeshes <- [];

    local SCENE_SIZE = 100;
    for(local i = 0; i < 10; i++){
        local source = _audio.newSourceFromBuffer(soundBuffer);
        local pos = _random.randVec3() * Vec3(SCENE_SIZE, 0, SCENE_SIZE);
        source.setPosition(pos);
        source.play();
        source.setLooping(true);
        ::audioSources.push(source);

        local mesh = _mesh.create("cube");
        mesh.setPosition(pos);
        ::audioMeshes.push(mesh);
    }
}