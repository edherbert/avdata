function windowMinimized(id, data){
    print("Window minimized");
}
function windowMaximized(id, data){
    print("Window maximized");
}
function windowFocusGained(id, data){
    print("Window focus gained");
}
function windowFocusLost(id, data){
    print("Window focus lost");
}
function windowShown(id, data){
    print("Window shown");
}
function windowHidden(id, data){
    print("Window hidden");
}
function windowRestored(id, data){
    print("Window restored");
}
function windowExposed(id, data){
    print("Window exposed");
}
function windowResized(id, data){
    print(format("Window resized width: %i height: %i", data.width, data.height));
    processResize();
}

function processResize(){
    _gui.setCanvasSize(Vec2(500, 500), _window.getActualSize());
}

function setMouseButtonsLabel(left, right){
    ::mouseButtonsLabel.setText(format("Mouse button left: %i, right: %i", left, right))
}
function setWindowPositionLabel(x, y){
    ::windowPositionLabel.setText(format("Window position X: %i, Y: %i", x, y))
}
function setupGui(){
    ::win <- _gui.createWindow();
    ::win.setPosition(0, 0);
    ::win.setSize(_window.getSize());
    ::win.setVisualsEnabled(false);

    ::titleCount <- 1;
    ::winPosX <- _window.getPositionX();
    ::winPosY <- _window.getPositionY();

    local layout = _gui.createLayoutLine();

    ::mouseButtonsLabel <- ::win.createLabel();
    setMouseButtonsLabel(_input.getMouseButton(0), _input.getMouseButton(1));
    layout.addCell(mouseButtonsLabel);

    ::windowPositionLabel <- ::win.createLabel();
    setWindowPositionLabel(::winPosX, ::winPosY);
    layout.addCell(windowPositionLabel);


    local labels = ["fullscreen window", "fullscreen borderless" "set title", "show message box", "Position window", "Toggle Borderless", "Alter Size", "Current Display Info"];
    local callbacks = [
        function(widget, action){
            local fullscreen = _window.getFullscreen();
            _window.setFullscreen(!fullscreen);
        },
        function(widget, action){
            local targetIdx = _window.getWindowDisplayIndex();
            local position = _window.getDisplayPositionCoordinates(targetIdx);
            local displaySize = _window.getDisplaySize(targetIdx);

            _window.setBorderless(true);
            _window.setSize(displaySize.x.tointeger(), displaySize.y.tointeger());
            _window.setPosition(position.x.tointeger(), position.y.tointeger());
        },
        function(widget, action){
            _window.setTitle("Window set title " + titleCount);
            ::titleCount++;

            print("Current title " + _window.getTitle());
        },
        function(widget, action){
            local pressedButton = _window.showMessageBox({
                "title": "This is a title",
                "message": "You can create a message box like this!",
                "buttons": [
                    "Show warning",
                    "Show error",
                    "Close",
                ]
            });

            if(pressedButton == 0){
                _window.showMessageBox({
                    "message": "This is a warning dialog!",
                    "flags": _MESSAGEBOX_WARNING
                });
            }
            else if(pressedButton == 1){
                _window.showMessageBox({
                    "message": "This is an error dialog!",
                    "flags": _MESSAGEBOX_ERROR
                });
            }
        },
        function(widget, action){
            _window.setPosition(0, 0);
        },
        function(widget, action){
            _window.setBorderless(!_window.getBorderless());
        },
        function(widget, action){
            _window.setSize(500, 800);
        },
        function(widget, action){
            local targetIdx = _window.getWindowDisplayIndex();
            local position = _window.getDisplayPositionCoordinates(targetIdx);
            local displaySize = _window.getDisplaySize(targetIdx);
            print(targetIdx);
            print(position);
            print(displaySize);
        }
    ];
    foreach(i,c in labels){
        local button = ::win.createButton();
        button.setText(c);
        button.attachListenerForEvent(callbacks[i], 2);
        button.setUserId(i);
        layout.addCell(button);
    }

    local layoutHorizontal = _gui.createLayoutLine(_LAYOUT_HORIZONTAL);
    local spinner = ::win.createSpinner();
    spinner.setOptions([
        "arrow",
        "ibeam",
        "wait",
        "crosshair",
        "waitarrow",
        "sizenwse",
        "sizenesw",
        "sizewe",
        "sizens",
        "sizeall",
        "no",
        "hand",
    ]);
    layoutHorizontal.addCell(spinner);
    ::cursorOptionSpinner <- spinner;
    local button = ::win.createButton();
    button.setText("Set cursor");
    button.attachListenerForEvent(function(widget, action){
        print("Setting cursor");
        _window.setCursor(::cursorOptionSpinner.getValueRaw());
    }, 2);
    layoutHorizontal.addCell(button);

    layout.addCell(layoutHorizontal);

    local displayText = ::win.createLabel();
    local numDisplays = _window.getNumDisplays();
    displayText.setText(format("Number of displays: %i", numDisplays));
    layout.addCell(displayText);

    layout.layout();
}

function start(){
    print("starting");

    setupGui();

    ::AcceptHandle <- _input.getButtonActionHandle("Accept");

    _event.subscribe(_EVENT_SYSTEM_WINDOW_MINIMIZED, windowMinimized, this);
    _event.subscribe(_EVENT_SYSTEM_WINDOW_MAXIMISED, windowMaximized, this);
    _event.subscribe(_EVENT_SYSTEM_WINDOW_FOCUS_GAINED, windowFocusGained, this);
    _event.subscribe(_EVENT_SYSTEM_WINDOW_FOCUS_LOST, windowFocusLost, this);
    _event.subscribe(_EVENT_SYSTEM_WINDOW_SHOWN, windowShown, this);
    _event.subscribe(_EVENT_SYSTEM_WINDOW_HIDDEN, windowHidden, this);
    _event.subscribe(_EVENT_SYSTEM_WINDOW_RESTORED, windowRestored, this);
    _event.subscribe(_EVENT_SYSTEM_WINDOW_EXPOSED, windowExposed, this);
    _event.subscribe(_EVENT_SYSTEM_WINDOW_RESIZE, windowResized, this);

}

function updateWindowPosition(){
    local newWinX = _window.getPositionX();
    local newWinY = _window.getPositionY();
    if(newWinX != ::winPosX || newWinX != ::winPosX){
        print("Window repositioned");
        ::winPosX = newWinX;
        ::winPosY = newWinY;

        setWindowPositionLabel(newWinX, newWinY);
    }
}

function update(){
    setMouseButtonsLabel(_input.getMouseButton(0), _input.getMouseButton(1));

    //TODO fix this so it doesn't crash everything!
    if(_input.getButtonAction(::AcceptHandle, _INPUT_PRESSED)){
        print("Button pressed");
        local fullscreen = _window.getFullscreen();
        _window.setFullscreen(!fullscreen);
    }

    updateWindowPosition();
}

function end(){
    print("ending");
}
