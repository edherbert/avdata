enum TextTypes{
    X_AND_Y,
    LEFT,
    RIGHT,
    LEFT_PRESSED,
    RIGHT_PRESSED,
    LEFT_RELEASED,
    RIGHT_RELEASED,

    MAX
};

function start(){
    _doFile(_settings.getDataDirectory() + "/../common/jankyGui/jankyText.nut");
    ::EntryType <- class{
        mEntryId = null;
        mText = null;
        mValue = false;
        mTargetText = null;
        constructor(entryId){
            mEntryId = entryId;
            mText = janky.text("");
            mText.setSize(16);
            mText.setPosition(0, 16 * entryId);
        }
        function update(){
            local text = "";
            if(mEntryId == TextTypes.X_AND_Y){
                text = format("X: %i Y: %i", _input.getMouseX(), _input.getMouseY());
            }
            else if(mEntryId == TextTypes.LEFT){
                if(_input.getMouseButton(_MouseButtonLeft)) text = "Left mouse down!";
            }
            else if(mEntryId == TextTypes.RIGHT){
                if(_input.getMouseButton(_MouseButtonRight)) text = "Right mouse down!";
            }
            else if(mEntryId == TextTypes.LEFT_PRESSED){
                if(_input.getMousePressed(_MouseButtonLeft)) text = "Left pressed!";
            }
            else if(mEntryId == TextTypes.RIGHT_PRESSED){
                if(_input.getMousePressed(_MouseButtonRight)) text = "Right pressed!";
            }
            else if(mEntryId == TextTypes.LEFT_RELEASED){
                if(_input.getMouseReleased(_MouseButtonLeft)) text = "Left released!";
            }
            else if(mEntryId == TextTypes.RIGHT_RELEASED){
                if(_input.getMouseReleased(_MouseButtonRight)) text = "Right released!";
            }
            mText.setText(text);
        }
    };

    ::entries <- array(TextTypes.MAX);
    for(local i = 0; i < TextTypes.MAX; i++){
        entries[i] = EntryType(i);
    }
}

function update(){
    foreach(i in ::entries){
        i.update();
    }
}

function end(){
    print("ending");
}
