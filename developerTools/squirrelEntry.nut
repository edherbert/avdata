function start(){
    print("starting");

    _camera.setPosition(0, 100, 100);
    _camera.lookAt(0, 0, 0);

    ::count <- 0;
    ::spinCount <- 0.0;
}

function update(){
    if(count < 150){
        //The function to draw debug shapes needs to be called each frame. This is easier than having to create an instance of something.
        //This just shows that if you stop calling them, they're removed.
        _developer.drawPoint(SlotPosition());

        _developer.drawAxis(SlotPosition(0, 0, 10, 0, 0), 1);

        _developer.drawCircle(SlotPosition(0, 0, 20, 0, 0), 5);

        _developer.drawSphere(SlotPosition(0, 0, 30, 0, 0), 5);
    }
    else if(count == 200) count = 0;

    //Spin the camera
    {
        const radius = 100;

        _camera.setPosition(cos(::spinCount)*radius, 100, sin(::spinCount)*radius);
        _camera.lookAt(0, 0, 0);
    }

    count++;
    spinCount += 0.01;
}

function end(){
    print("ending");
}
