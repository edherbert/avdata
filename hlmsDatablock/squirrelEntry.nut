function start(){
    _doFile("res://../common/scripts/fpsCamera.nut");

    ::datablocks <- [];
    ::meshes <- [];

    {
        local datablock = _hlms.getDatablock("woodMaterial");
        ::datablocks.append(datablock);
    }
    {
        local constructionInfo = {
            "diffuse": "255 0 0"
        };
        local datablock = _hlms.pbs.createDatablock("datablockDiffuse", null, null, constructionInfo);
        ::datablocks.append(datablock);
    }
    {
        local constructionInfo = {
            "diffuse": "0 0 1"
        };
        local datablock = _hlms.unlit.createDatablock("datablockUnlit", null, null, constructionInfo);
        ::datablocks.append(datablock);
    }
    {
        local macroblock = _hlms.getMacroblock({
            "polygonMode": 1,
        });
        local constructionInfo = {
            "diffuse": "1 0 0"
        };
        local datablock = _hlms.pbs.createDatablock("datablockWireframe", null, macroblock, constructionInfo);
        ::datablocks.append(datablock);
    }
    {
        local datablock = _hlms.getDatablock("waterMaterial");
        ::datablocks.append(datablock);
    }

    foreach(c,i in ::datablocks){
        local mesh = _mesh.create("materialball.mesh");
        mesh.setScale(2, 2, 2);
        local pos = SlotPosition();
        pos.move(35 * c, 0, 0);
        mesh.setPosition(pos);

        mesh.setDatablock(i);
        ::meshes.append(mesh);
    }

    ::rec <- Rect2d();
    rec.setSize(200, 200);
    ::rec.setDatablock(_hlms.getDatablock("colourExample"));

    _camera.setPosition(0, 0, 150);
    _camera.lookAt(0, 0, 0);

    ::fpsCamera.start(_camera.getDirection());
}

function update(){
    ::fpsCamera.update();
}
