function start(){
    print("starting");

    _doFile("res://../common/scripts/fpsCamera.nut");

    _world.createWorld();

    ::mesh <- _mesh.create("ogrehead2.mesh");
    ::meshPos <- SlotPosition();
    ::animCount <- 0.0;

    //Mesh created by string.
    ::entity1 <- _entity.create(SlotPosition(1, 0, 0, 0, 0));
    _component.mesh.add(entity1, "ogrehead2.mesh");

    //Created by passing a mesh instance.
    ::entity2 <- _entity.create(SlotPosition(-1, 0, 0, 0, 0));
    ::entity2Mesh <- _mesh.create("ogrehead2.mesh");
    _component.mesh.add(entity2, entity2Mesh);

    _camera.setPosition(0, 0, 300);
    _camera.lookAt(0, 0, 0);

    ::fpsCamera.start(_camera.getDirection());
}

function update(){
    ::fpsCamera.update();
    animCount+=0.01;
    local val = cos(animCount);

    meshPos.move(0, cos(animCount), 0);
    ::mesh.setPosition(meshPos);

    //Getting a mesh created from the string construction.
    local entity1Mesh = _component.mesh.getMesh(entity1);
    entity1Mesh.setScale(val, val, val);

    ::entity2Mesh.setOrientation(val, 0, 0, 1);
}

function end(){
    print("ending");
}
