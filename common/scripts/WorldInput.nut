function basicWorldInput(){
    if( !("first" in this) ){
        this.first <- false;
        this.inputHandle <- _input.getAxisActionHandle("LeftMove");
    }

    local pos = _world.getPlayerPosition();

    local ammount = [
        _input.getAxisActionX(this.inputHandle) * 3,
        0,
        _input.getAxisActionY(this.inputHandle) * 3
    ];

    //Don't do all this movement logic if there was no movement.
    //if(ammount[0] == 0 && ammount[2] == 0) return;

    pos.move(ammount[0], ammount[1], ammount[2]);

    _world.setPlayerPosition(pos);

    local playerPos = pos.toVector3();
    _camera.setPosition(playerPos[0], playerPos[1] + 200, playerPos[2] + 200);
}
