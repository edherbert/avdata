function start(){
    getroottable().x <- 0;
}

function main(){
    if("x" in getroottable()){
        update();
    }else{
        start();
    }
}

function update(){
    local angle = getroottable().x;
    print(getroottable().x);

    local radius = 100;
    local xPos = cos(angle)*radius;
    local yPos = sin(angle)*radius;

    print("xPos" + xPos);
    camera.setPosition(xPos, 0, yPos);
    //camera.setPosition(0, 0, 100);
    camera.lookAt(0, 0, 0);
    getroottable().x = angle + 0.01;
}

main();
