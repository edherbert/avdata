::janky <- {

}


::janky.text <- class{
    textString = null;
    textImages = null;

    fontSize = null;
    posX = null;
    posY = null;

    textLayer = null;

    static jankyTextString1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";
    static jankyTextString2 = "0123456789.,!?'\"-+=/\\%()<>:;";

    constructor(s){
        textString = "";
        textImages = [];

        fontSize = 32;
        posX = 0;
        posY = 0;
        textLayer = 0;

        setText(s);
    }

    function setText(s){
        textString = s.toupper();

        _configureText();
    }

    function setPosition(x, y){
        posX = x;
        posY = y;

        _repositionText();
    }

    function setSize(size){
        fontSize = size;

        _resizeText();
        _repositionText();
    }

    function setLayer(layer){
        foreach(i in textImages){
            i.setLayer(layer);
        }
        textLayer = layer;
    }

    //Private

    function _repositionText(){
        foreach(idx,tex in textImages){
            tex.setPosition(posX + idx * fontSize, posY);
        }
    }

    function _resizeText(){
        foreach(tex in textImages){
            tex.setSize(fontSize, fontSize);
        }
    }

    function _recalculateChars(){
        for(local i = 0; i < textString.len(); i++){

            local line1 = true;
            local t = jankyTextString1.find(textString.slice(i, i + 1));
            if(t == null){ //Not found on the first row.
                t = jankyTextString2.find(textString.slice(i, i + 1));
                line1 = false;
            }
            if(t == null) t = 13; //If nothing was found, 13 is used, which is the question mark.

            local target = t;
            textImages[i].setSectionScale(8.0 / 256.0, 0.5, (target * 8.0) / 256.0, line1 ? 0.0 : 0.5);
        }
    }

    function _configureText(){
        {
            //Determine the number of characters in the string, versus how many are in the array.
            local textLen = textString.len();
            local arrayLen = textImages.len();
            if(textLen > arrayLen){
                //There aren't enough images in the list, so some need to be created.

                for(local i = 0; i < textLen - arrayLen; i++){
                    local tex = MovableTexture("font.png");
                    tex.setLayer(textLayer);

                    textImages.push(tex);
                }

            }else if(textLen < arrayLen){
                textImages.resize(textLen);

            } //If they are equal do nothing.

        }

        //We should now have the correct number of textures in the list.

        _resizeText();
        _repositionText();
        _recalculateChars();

    }
}
