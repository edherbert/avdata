function start(){
    print("starting");

    //The world needs to be created for shapes to be created.
    _world.createWorld();
    ::init <- false;
    _camera.setPosition(0, 0, 300);
    _camera.lookAt(0, 0, 0);
}

function update(){
    if(!init){
    	local first = _physics.getCubeShape(1, 1, 1);
    	local second = _physics.getSphereShape(10);

        local constructionInfo = {
            "mass": 1,
            "friction":5,
            "rollingFriction":6.1,
            "spinningFriction":6.2,
            "restitution":2,
            "origin": [10, 20, 60],
            "rotation": [0, 0, 0, 1]
        };
        ::body <- _physics.dynamics.createRigidBody(first, constructionInfo);
        //local body = _physics.dynamics.createRigidBody(first);

        _physics.dynamics.addBody(body);

        //Create an entity with this rigid body.
        local en = _entity.create(SlotPosition());
        _component.mesh.add(en, "ogrehead2.mesh");
        _component.rigidBody.add(en, body);

        init = true;
    }
}

function end(){
    print("ending");
}
