::totalClicks <- 0;


function widgetCallback(widget, action){
    local pressed = (action == _GUI_ACTION_PRESSED);
    assert(pressed);
    print("pressed: " + pressed.tostring());

    totalClicks++;
    print(totalClicks);
}

function editboxCallback(widget, action){
    print("editbox " + action);
    print(widget.getText());
}

::setSliderValue <- function(widget, value){
    sliderLabel.setText("Slider Value: " + value);
}
function sliderCallback(widget, action){
    print(action);
    ::setSliderValue(widget, widget.getValue());
}

function checkboxCallback(widget, action){
    print("ticked: " + widget.getValue());
}

function swapOrderCallback(widget, action){
    ::currentWindowOrder = !currentWindowOrder;
    ::window1.setZOrder(currentWindowOrder ? 5 : 10);
    ::window2.setZOrder(currentWindowOrder ? 10 : 5);
    print("Current z order: " + currentWindowOrder);
}

function second(){
    local x = 10;
    local y = 20;

    //::something = 20;
}

function first(){
    local thing = "some text";
    second();

    ::somethingElse = 100;
}

function start(){
    local layout = _gui.createLayoutLine();

    local win = _gui.createWindow();
    win.setPosition(600, 50);
    win.setSize(500, 1000);
    win.setHidden(false);

    local firstButton = win.createButton();
    firstButton.setText("A button");
    layout.addCell(firstButton);

    local secondButton = win.createButton();
    secondButton.setDefaultFont(1);
    secondButton.setText("Different font");
    layout.addCell(secondButton);

    local thirdButton = win.createButton();
    thirdButton.setSkin("internal/ButtonSkin", _GUI_WIDGET_STATE_IDLE);
    thirdButton.setText("Idle skin button");
    layout.addCell(thirdButton);

    local fourthButton = win.createButton();
    fourthButton.setSkinPack("internal/ButtonSkin");
    fourthButton.setText("Button skin pack");
    layout.addCell(fourthButton);

    foreach(i in ["Some text", "これは本当の人生なのか", "これはただの幻想なのか"]){
        local label = win.createLabel();
        label.setText(i);
        layout.addCell(label);
    }

    local shadowLabel = win.createLabel();
    shadowLabel.setText("A label with shadow");
    shadowLabel.setShadowOutline(true);
    layout.addCell(shadowLabel);

    local secondShaodwLabel = win.createLabel();
    secondShaodwLabel.setText("A label with fancy shadow");
    secondShaodwLabel.setShadowOutline(true, ColourValue(1, 0, 0, 1), Vec2(2, 3));
    layout.addCell(secondShaodwLabel);

    local shadowButton = win.createButton();
    shadowButton.setText("Shadow button");
    shadowButton.setShadowOutline(true, ColourValue(1, 0, 1, 1), Vec2(2, 3));
    layout.addCell(shadowButton);

    local editbox = win.createEditbox();
    editbox.setSize(300, 100);
    editbox.setText("This is an editbox");
    layout.addCell(editbox);

    local slider = win.createSlider();
    slider.setSize(300, 32);
    //slider.setValue(0.5);
    layout.addCell(slider);

    ::sliderLabel <- win.createLabel();
    sliderLabel.setDefaultFont(1);
    ::setSliderValue(slider, 0);
    layout.addCell(sliderLabel);

    local checkbox = win.createCheckbox();
    checkbox.setText("This is a checkbox");
    layout.addCell(checkbox);

    //
    local newButton = win.createButton();
    newButton.setText("test button");
    layout.addCell(newButton);

    local swapOrder = win.createButton();
    swapOrder.setText("swap order");
    layout.addCell(swapOrder);

    //Panels are simple enough that a datablock can be applied to them easily.
    local datablockWindow = win.createPanel();
    datablockWindow.setSkinPack("Empty");
    datablockWindow.setSize(200, 200);
        local datablockForWindow = _hlms.unlit.createDatablock("datablockWindow");
        datablockForWindow.setTexture(0, "cat1.jpg");
    datablockWindow.setDatablock(datablockForWindow);
    layout.addCell(datablockWindow);

    local richText = win.createLabel();
    richText.setText("some rich text");
    richText.setRichText([
        {"fontSize":10.0, "offset": 0, "len": 4},
        {"fontSize":20.0, "offset": 5, "len": 4, "col": "1 0 0 1"},
        {"fontSize":40.0, "offset": 10, "len": 4, "font": 1},
    ]);
    richText.sizeToFit();
    layout.addCell(richText);

    ::animLabel <- win.createAnimatedLabel();
    local animLabelText = "Animated text";
    ::animLabelLength <- animLabelText.len();
    animLabel.setText(animLabelText);
    layout.addCell(animLabel);

    ::count <- 0.0;

    layout.layout();

    firstButton.attachListenerForEvent(widgetCallback, _GUI_ACTION_PRESSED, this);
    editbox.attachListener(editboxCallback);
    slider.attachListener(sliderCallback);
    checkbox.attachListenerForEvent(checkboxCallback, _GUI_ACTION_RELEASED);
    swapOrder.attachListenerForEvent(swapOrderCallback, _GUI_ACTION_PRESSED);

    local context = {
        "x":10,
        function testFunction(a, b){
            x++;
            print("hello this is a test " + x);
        }
    }
    newButton.attachListener(context.testFunction, context);

    //Re-orderable windows.
    ::window1 <- _gui.createWindow();
    ::window2 <- _gui.createWindow();
    ::window1.setSize(100, 100);
    ::window2.setSize(100, 100);
    ::window1.setPosition(325, 325);
    ::window2.setPosition(350, 350);
    ::currentWindowOrder <- false;
    ::window1.setZOrder(currentWindowOrder ? 5 : 10);
    ::window2.setZOrder(currentWindowOrder ? 10 : 5);

    local win1lab = ::window1.createLabel();
    local win2lab = ::window2.createLabel();
    win1lab.setText("front");
    win2lab.setText("back");

    // This is the default keymap anyway, but just to show how to map gui inputs.
    _gui.mapKeyboardInput(_K_UP, _GUI_INPUT_TOP);
    _gui.mapKeyboardInput(_K_DOWN, _GUI_INPUT_BOTTOM);
    _gui.mapKeyboardInput(_K_LEFT, _GUI_INPUT_LEFT);
    _gui.mapKeyboardInput(_K_RIGHT, _GUI_INPUT_RIGHT);
    _gui.mapKeyboardInput(_K_RETURN, _GUI_INPUT_PRIMARY);

    _gui.mapControllerInput(_B_A, _GUI_INPUT_PRIMARY);
    _gui.mapControllerInput(_B_DPADUP, _GUI_INPUT_TOP);
    _gui.mapControllerInput(_B_DPADDOWN, _GUI_INPUT_BOTTOM);
    _gui.mapControllerInput(_B_DPADLEFT, _GUI_INPUT_LEFT);
    _gui.mapControllerInput(_B_DPADRIGHT, _GUI_INPUT_RIGHT);

    _gui.mapControllerAxis(_BA_LEFT, _GUI_INPUT_TOP, _GUI_INPUT_BOTTOM, _GUI_INPUT_LEFT, _GUI_INPUT_RIGHT);

}

function update(){
    count += 0.05;

    for(local i = 0; i < ::animLabelLength; i++){
        animLabel.setAnimatedGlyph(i, 0.0, sin(count + i * 0.5)*0.01, 0xffffffff);
    }
}
