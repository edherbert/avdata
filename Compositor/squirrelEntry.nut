enum WorkspaceTypes{
    Default,
    RenderToTexture,
    VisibilityMask,
    RenderQueue,
    GlitchEffect
};

function widgetCallback(widget, action){
    if(action != 2) return;

    local idx = widget.getUserId();
    setWorkspaceDemo(idx)
}

function createDefaultWorkspace(){
    local renderTexture = _window.getRenderTexture();
    local defaultWorkspace = _compositor.addWorkspace([renderTexture], _camera.getCamera(), "default_finalWorkspace", false);

    ::workspaces[WorkspaceTypes.Default] <- [defaultWorkspace];
}

function workspaceRenderSceneToTexture(){
    local newTex = _graphics.createTexture("someTexture");
    newTex.setResolution(500, 500);
    newTex.scheduleTransitionTo(_GPU_RESIDENCY_RESIDENT);
    local renderTextureFirst = _compositor.addWorkspace([newTex], _camera.getCamera(), "SceneToTexture_renderToTexture", false);

    local renderTexture = _window.getRenderTexture();
    local renderTextureSecond = _compositor.addWorkspace([renderTexture, newTex], _camera.getCamera(), "SceneToTexture_finalWorkspace", false);

    ::workspaces[WorkspaceTypes.RenderToTexture] <- [renderTextureFirst, renderTextureSecond];
}

function createWorkspaceVisibilityMask(){
    local renderTexture = _window.getRenderTexture();
    local visibilityMaskWorkspace = _compositor.addWorkspace([renderTexture], _camera.getCamera(), "visibilityMask_finalWorkspace", false);

    ::workspaces[WorkspaceTypes.VisibilityMask] <- [visibilityMaskWorkspace];
}

function createWorkspaceRenderQueues(){
    local renderTexture = _window.getRenderTexture();
    local renderQueueWorkspace = _compositor.addWorkspace([renderTexture], _camera.getCamera(), "renderQueue_finalWorkspace", false);

    ::workspaces[WorkspaceTypes.RenderQueue] <- [renderQueueWorkspace];
}

function createWorkspaceGlitchEffect(){
    local renderTexture = _window.getRenderTexture();
    local renderQueueWorkspace = _compositor.addWorkspace([renderTexture], _camera.getCamera(), "glitchEffect_workspace", false);

    ::workspaces[WorkspaceTypes.GlitchEffect] <- [renderQueueWorkspace];
}

::setWorkspaceDemo <- function(id){
    foreach(i,c in ::workspaces){
        foreach(y in c){
            y.setEnabled(i == id);
        }
    }

    local data = ::workspaces[id];
    foreach(i in data){
        i.setEnabled(true);
    }
}

function setupGui(){
    ::win <- _gui.createWindow();
    ::win.setPosition(0, 0);
    ::win.setSize(500, 500);

    local layout = _gui.createLayoutLine();
    local labels = ["Default", "renderToTexture", "visibilityMask", "renderQueue", "glitchEffect"];
    foreach(i,c in labels){
        local button = ::win.createButton();
        button.setText(c);
        button.attachListener(widgetCallback);
        button.setUserId(i);
        layout.addCell(button);
    }
    layout.layout();
}

function setupScene(){
    ::meshNode <- _scene.getRootSceneNode().createChildSceneNode();
    ::meshNode2 <- _scene.getRootSceneNode().createChildSceneNode();

    ::meshObject <- _scene.createItem("cube");
    ::meshObject.setVisibilityFlags(0x1);
    ::meshObject.setRenderQueueGroup(1);
    ::meshObject2 <- _scene.createItem("cube");
    ::meshObject2.setVisibilityFlags(0x2);
    ::meshObject2.setRenderQueueGroup(10);

    ::meshNode.attachObject(meshObject);
    ::meshNode2.attachObject(meshObject2);

    ::meshNode.setPosition(5, 0, 0);
    ::meshNode2.setPosition(-5, 0, 0);
}

function start(){

    // local camera = _scene.createCamera("first");
    // local child = _scene.getRootSceneNode().createChildSceneNode();
    // child.attachObject(camera);
    // child.setPosition(0, 0, 20);
    // camera.lookAt(0, 0, 0);

    ::workspaces <- {};

    _doFile("res://../common/scripts/fpsCamera.nut");

    _camera.setPosition(0, 0, 20);
    _camera.lookAt(0, 0, 0);

    ::fpsCamera.start(_camera.getDirection());

    createDefaultWorkspace();
    workspaceRenderSceneToTexture();
    createWorkspaceVisibilityMask();
    createWorkspaceRenderQueues();
    createWorkspaceGlitchEffect();

    setWorkspaceDemo(WorkspaceTypes.Default);
    setupScene();

    ::count <- 0;

    local winSize = Vec2(_window.getWidth(), _window.getHeight());
    _gui.setCanvasSize(winSize, winSize);

    setupGui();
}

function update(){
    ::fpsCamera.update();

    count += 0.01;
    if(count >= 1) count = 0;
    ::meshNode.setOrientation(Quat(count, 0, 0, 1));
    ::meshNode2.setOrientation(Quat(0, count, 0, 1));
}
