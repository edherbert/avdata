//https://www.shadertoy.com/view/lsXSWl

#include <metal_stdlib>
using namespace metal;

struct PS_INPUT
{
	float2 uv0;
};

struct Params
{
	float distortionFreq;
	float distortionScale;
	float distortionRoll;
	float interference;
	float frameLimit;
	float frameShape;
	float frameSharpness;
	float time_0_X;
	float sin_time_0_X;
};

float rng2(float2 seed, float iTime)
{
    return fract(sin(dot(seed * floor(iTime * 12.), float2(127.1,311.7))) * 43758.5453123);
}

float rng(float seed, float iTime)
{
    return rng2(float2(seed, 1.0), iTime);
}

fragment float4 main_metal
(
	PS_INPUT inPs [[stage_in]],
	texture2d<float>	Image			[[texture(0)]],
	sampler				samplerState	[[sampler(0)]],

	constant Params &p [[buffer(PARAMETER_SLOT)]]
)
{
   float2 uv = inPs.uv0;
   float2 blockS = floor(uv * float2(24., 9.));
   float2 blockL = floor(uv * float2(8., 4.));
   float iTime = p.time_0_X;

   float lineNoise = pow(rng2(blockS, iTime), 8.0) * pow(rng2(blockL, iTime), 3.0) - pow(rng(7.2341, iTime), 17.0) * 2.;

   float4 col1 = Image.sample(samplerState, uv);
   float4 col2 = Image.sample(samplerState, uv + float2(lineNoise * 0.03 * rng(5.0, iTime), 0));
   float4 col3 = Image.sample(samplerState, uv - float2(lineNoise * 0.03 * rng(31.0, iTime), 0));

   return float4(float3(col1.x, col2.y, col3.z), 1.0);
}
