//We had to write a perceptron for our neural networks unit, so I did it in squirrel.

function start(){
    print("starting");

    local inputTable = {
        "x":
        [
            [1, 0, 0],
            [1, 1, 0],
            [1, 0, 1],
            [1, 1, 1],
        ],

        "w": [-0.2, 0.3, -0.2],
        "t": [0, 0, 0, 1],
        "learningRate": 0.35
    };

    local finishedConverging = false;
    for(local i = 0; i < 6; i++){
        local result = performEpoch(inputTable);
        if(result){
            finishedConverging = true;
            break;
        }
    }

    print(finishedConverging ? "It converged" : "It didn't converge :(");
    print("===Final Weights===");
    print("    " + inputTable.w[0]);
    print("    " + inputTable.w[1]);
    print("    " + inputTable.w[2]);
}

function update(){

}

function end(){
    print("ending");
}

function performEpoch(input){
    local s = [];

    for(local i = 0; i < 4; i++){
        local currentX = input.x[i];

        local weightedSum = 0;
        for(local y = 0; y < currentX.len(); y++){
            weightedSum += currentX[y] * input.w[y];
        }
        //print("Weighted Sum: " + weightedSum);

        local output = heavySide(weightedSum);

        local currentS = [];
        for(local y = 0; y < currentX.len(); y++){
            local diff = input.t[i] - output;
            local targetS = 0;
            if(diff > 0) targetS = currentX[y];
            if(diff < 0) targetS = -currentX[y];
            currentS.append(targetS);
        }
        s.append(currentS);
    }

    /*foreach(i in s){
        foreach(y in i){
            print(y);
        }
    }*/

    print("");
    //First of all, determine s. That way if it's zero we can stop early.
    local vs = [];
    local allZero = true;
    for(local i = 0; i < 3; i++){
        local verticalSum = 0;
        foreach(y in s){
            verticalSum += y[i];
        }
        vs.append(verticalSum);

        if(verticalSum != 0) allZero = false;
    }

    if(allZero) return true; //The correct weights have been found and there's nothing more to do.

    for(local i = 0; i < 3; i++){
        local dw = vs[i] * input.learningRate;

        local newWeight = dw + input.w[i];
        print("new weight: " + newWeight);

        input.w[i] = newWeight;
    }

    //End of the epoch and the weights didn't match.
    return false
}

function heavySide(num){
    if(num > 0) return 1;

    return 0;
}
