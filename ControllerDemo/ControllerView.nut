this.ControllerView <- class{
    controllerImg = null;
    buttonImages = null;
    stickImages = null;

    controllerText = null;
    leftTriggerText = null;
    rightTriggerText = null;

    leftStickDb = null;
    rightStickDb = null;

    hapticButtons = null;
    hapticWindow = null;

    mId = 0;

    mX = 0;
    mY = 0;
    mWidth = 0;
    mHeight = 0;

    leftTriggerVal = -1.0;
    rightTriggerVal = -1.0;

    constructor(id){
        this.mId = id;
        this.buttonImages = [];
        this.stickImages = [];
        this.hapticButtons = [];

        leftStickDb = _hlms.getDatablock("leftStickDb" + id);
        if(leftStickDb == null) leftStickDb = _hlms.unlit.createDatablock("leftStickDb" + id, null, null, {"diffuse": "0.4 1.0 0.4"});
        rightStickDb = _hlms.getDatablock("rightStickDb" + id);
        if(rightStickDb == null) rightStickDb = _hlms.unlit.createDatablock("rightStickDb" + id, null, null, {"diffuse": "0.4 1.0 0.4"});

        //Uses textures by xUMR from open game art.
        controllerImg = MovableTexture("xboxOne.png");
        //controllerImg = MovableTexture("ps4Controller.png");
        controllerImg.setLayer(10);

        leftTriggerText = janky.text("");
        rightTriggerText = janky.text("");
        leftTriggerText.setSize(16);
        rightTriggerText.setSize(16);

        positionControllerImage();

        local deviceName = _input.getDeviceName(id);
        controllerText = janky.text(deviceName);
        print("Controller id " + id + ": " + deviceName);
        controllerText.setPosition(mX + 10, mY + 10);

        produceXBoxOneButtonHighlights();
        produceGuiRumbleButtons();
    }

    function shutdown(){
        _gui.destroy(this.hapticWindow);
        hapticButtons.clear();
    }

    function produceGuiRumbleButtons(){
        local win = _gui.createWindow();
        win.setPosition(mX, mY + (mHeight * 0.4));
        win.setSize(mWidth, mHeight);
        win.setVisualsEnabled(false);
        win.setHidden(true);

        this.hapticWindow = win;

        local layout = _gui.createLayoutLine();

        local hapticOptions = ["Weak rumble", "Strong rumble", "Long rumble"];
        local callbacks = [
            function(widget, action){
                local i = widget.getUserId();
                print(format("Weak rumble for device %i", i));
                _input.rumbleInputDevice(i, 0.2, 0.2, 500);
            },
            function(widget, action){
                local i = widget.getUserId();
                print(format("Strong rumble for device %i", i));
                _input.rumbleInputDevice(i, 1.0, 1.0, 500);
            },
            function(widget, action){
                local i = widget.getUserId();
                print(format("Long rumble for device %i", i));
                _input.rumbleInputDevice(i, 0.8, 0.8, 2000);
            },
        ];
        foreach(i,c in hapticOptions){
            local button = win.createButton();
            button.setText(c);
            button.attachListenerForEvent(callbacks[i], 2);
            button.setUserId(this.mId);
            layout.addCell(button);
        }
        layout.layout();
    }

    function positionControllerImage(){
        local winHalfWidth = _window.getWidth() / 2;
        local winHalfHeight = _window.getHeight() / 2;
        if(mId == 1 || mId == 3) mX = winHalfWidth;
        if(mId == 2 || mId == 3) mY = winHalfHeight;

        mWidth = winHalfWidth;
        mHeight = mWidth * 0.6666;
        controllerImg.setPosition(mX, mY);
        controllerImg.setSize(mWidth, mHeight);

        leftTriggerText.setPosition(mX + 10, mY + mHeight * 0.95);
        rightTriggerText.setPosition(mX + 10, mY + 20 + mHeight * 0.95);
    }

    function setButtonVisible(id, visible){
        if(id == 13 || id == 14){
            //One of the stick buttons. In this case just change the colour of the datablock.
            local target = id == 13 ? leftStickDb : rightStickDb;
            if(visible) target.setColour(0.6, 0.1, 0.1, 1.0);
            else target.setColour(0.4, 1.0, 0.4, 1.0);
            return;
        }
        this.buttonImages[id].setVisible(visible);
        showHapticButtons();
    }

    function showHapticButtons(){
        hapticWindow.setHidden(false);
    }

    function setTriggerAxis(leftTrigger, value){
        if(leftTrigger){
            if(value == leftTriggerVal) return;
            leftTriggerText.setText("Left Trigger: " + value);
            leftTriggerVal = value;
        }else{
            if(value == rightTriggerVal) return;
            rightTriggerText.setText("Right Trigger: " + value);
            rightTriggerVal = value;
        }
        showHapticButtons();
    }

    function setStickPosition(leftStick, x, y){
        local idx = leftStick ? 0 : 1;
        local targetImg = this.stickImages[idx];
        targetImg.setVisible(true);

        local targetStartX = leftStick ? 0.247 : 0.544;
        local targetStartY = leftStick ? 0.247 : 0.432;
        targetImg.setPosition(mX + targetStartX * mWidth + x * 50, mY + targetStartY * mHeight + y * 50);
    }

    function produceXBoxOneButtonHighlights(){
        local posData = [
            //ABXY
            [0.676, 0.376, 0.065, 0.082], //A
            [0.731, 0.305, 0.065, 0.082], //B
            [0.622, 0.296, 0.065, 0.082], //X
            [0.679352, 0.226, 0.065, 0.076], //Y

            //Dpad
            [0.392, 0.446, 0.035, 0.05],
            [0.392, 0.546, 0.035, 0.05],
            [0.426, 0.503, 0.035, 0.05],
            [0.356, 0.503, 0.035, 0.05],

            //Bumpers
            [0.233, 0.117, 0.145, 0.09],
            [0.609, 0.113, 0.145, 0.09],

            //Start, select, guide
            [0.43, 0.318, 0.05, 0.05],
            [0.545, 0.315, 0.05, 0.05],
            [0.46, 0.15, 0.1, 0.14],
        ];

        foreach(i in posData){
            local rec = Rect2d();
            rec.setLayer(5);
            rec.setPosition(mX + i[0] * mWidth, mY + i[1] * mHeight);
            rec.setSize(i[2] * mWidth, i[3] * mHeight);
            rec.setDatablock(::highlightDatablock);
            rec.setVisible(false);
            this.buttonImages.append(rec);
        }
        for(local i = 0; i < 2; i++){
            local rec = Rect2d();
            rec.setLayer(5);
            rec.setSize(0.116 * mWidth, 0.172 * mHeight);
            rec.setVisible(false);
            this.stickImages.append(rec);
        }
        this.stickImages[0].setDatablock(leftStickDb);
        this.stickImages[1].setDatablock(rightStickDb);
    }
};
