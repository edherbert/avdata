function deviceAdded(id, data){
    print("Device Added");
    createControllers();
}
function deviceRemoved(id, data){
    print("Device Removed");
    createControllers();
}

function start(){
    _doFile("res://ControllerView.nut");
    _doFile("res://../common/jankyGui/jankyText.nut");

    _event.subscribe(_EVENT_SYSTEM_INPUT_DEVICE_ADDED, deviceAdded, this);
    _event.subscribe(_EVENT_SYSTEM_INPUT_DEVICE_REMOVED, deviceRemoved, this);

    ::highlightDatablock <- _hlms.unlit.createDatablock("highlightButtonDatablock", null, null, {"diffuse": "0.4 1.0 0.4"});

    /**
    Note.
    The input system allows you to create action sets and actions to receive input into.
    This is sort of a bad example, as all the descriptions here are the names of the buttons they map to.
    Really, you would instead create actions named things like 'jump', 'run', etc, rather than controller button names.
    Then you map a controller button to that action as below, and just read the value from it.
    They're named after the buttons in this example because it's really intended to visualise the controllers themselves.
    */
    _input.setActionSets({
        "default" : {
            "Buttons" : {
                "AButton": "#AButton",
                "BButton": "#BButton",
                "XButton": "#XButton",
                "YButton": "#YButton",

                "DPadUp": "#DPadUp",
                "DPadDown": "#DPadDown",
                "DPadLeft": "#DPadLeft",
                "DPadRight": "#DPadRight",

                "LeftBumper": "#LeftBumper",
                "RightBumper": "#RightBumper",

                "LeftStickButton": "#LeftStickButton",
                "RightStickButton": "#RightStickButton",

                "StartButton": "#StartButton",
                "SelectButton": "#SelectButton"
                "GuideButton": "#GuideButton"
            },
            "AnalogTrigger":{
                "leftTrigger":"#LeftTrigger",
                "rightTrigger":"#RightTrigger"
            },
            "StickPadGyro" : {
                "leftStick":"#LeftStick",
                "rightStick":"#RightStick"
            }
        }
    });

    ::AButton <- _input.getButtonActionHandle("AButton");
    ::BButton <- _input.getButtonActionHandle("BButton");
    ::XButton <- _input.getButtonActionHandle("XButton");
    ::YButton <- _input.getButtonActionHandle("YButton");

    ::DPadUp <- _input.getButtonActionHandle("DPadUp");
    ::DPadDown <- _input.getButtonActionHandle("DPadDown");
    ::DPadLeft <- _input.getButtonActionHandle("DPadLeft");
    ::DPadRight <- _input.getButtonActionHandle("DPadRight");

    ::LeftBumper <- _input.getButtonActionHandle("LeftBumper");
    ::RightBumper <- _input.getButtonActionHandle("RightBumper");

    ::LeftStickButton <- _input.getButtonActionHandle("LeftStickButton");
    ::RightStickButton <- _input.getButtonActionHandle("RightStickButton");

    ::LeftStickButton <- _input.getButtonActionHandle("LeftStickButton");
    ::RightStickButton <- _input.getButtonActionHandle("RightStickButton");

    ::StartButton <- _input.getButtonActionHandle("StartButton");
    ::SelectButton <- _input.getButtonActionHandle("SelectButton");
    ::GuideButton <- _input.getButtonActionHandle("GuideButton");

    ::LeftTrigger <- _input.getTriggerActionHandle("leftTrigger");
    ::RightTrigger <- _input.getTriggerActionHandle("rightTrigger");

    ::LeftStick <- _input.getAxisActionHandle("leftStick");
    ::RightStick <- _input.getAxisActionHandle("rightStick");

    //Buttons
    _input.mapControllerInput(_B_A, ::AButton);
    _input.mapControllerInput(_B_B, ::BButton);
    _input.mapControllerInput(_B_X, ::XButton);
    _input.mapControllerInput(_B_Y, ::YButton);

    _input.mapControllerInput(_B_DPADUP, ::DPadUp);
    _input.mapControllerInput(_B_DPADDOWN, ::DPadDown);
    _input.mapControllerInput(_B_DPADLEFT, ::DPadLeft);
    _input.mapControllerInput(_B_DPADRIGHT, ::DPadRight);

    _input.mapControllerInput(_B_LEFTSHOULDER, ::LeftBumper);
    _input.mapControllerInput(_B_RIGHTSHOULDER, ::RightBumper);

    _input.mapControllerInput(_B_START, ::StartButton);
    _input.mapControllerInput(_B_BACK, ::SelectButton);
    _input.mapControllerInput(_B_GUIDE, ::GuideButton);
    _input.mapControllerInput(_B_TOUCHPAD, ::GuideButton);

    _input.mapControllerInput(_B_LEFTSTICK, ::LeftStickButton);
    _input.mapControllerInput(_B_RIGHTSTICK, ::RightStickButton);

    //Triggers
    _input.mapControllerInput(_BT_LEFT, ::LeftTrigger);
    _input.mapControllerInput(_BT_RIGHT, ::RightTrigger);

    //Axis
    _input.mapControllerInput(_BA_LEFT, ::LeftStick);
    _input.mapControllerInput(_BA_RIGHT, ::RightStick);


    //Keyboard. I've only mapped a few things for demonstration purposes.
    _input.mapKeyboardInput(_K_Z, ::AButton);
    _input.mapKeyboardInput(_K_X, ::BButton);

    //map m to the left trigger.
    _input.mapKeyboardInput(_K_M, ::LeftTrigger);

    //Map wasd to the left stick.
    _input.mapKeyboardInputAxis(_K_D, _K_S, _K_A, _K_W, ::LeftStick);

    ::controllerViews <- [];
    createControllers();
}

function createControllers(){
    foreach(i in controllerViews){
        i.shutdown();
    }
    ::controllerViews.clear();
    for(local i = 0; i < _MAX_INPUT_DEVICES; i++){
        controllerViews.append(ControllerView(i));
    }
}

function update(){
    for(local i = 0; i < _input.getNumControllers(); i++){
        local buttonData = [
            _input.getButtonAction(::AButton, _INPUT_ANY, i),
            _input.getButtonAction(::BButton, _INPUT_ANY, i),
            _input.getButtonAction(::XButton, _INPUT_ANY, i),
            _input.getButtonAction(::YButton, _INPUT_ANY, i),

            _input.getButtonAction(::DPadUp, _INPUT_ANY, i),
            _input.getButtonAction(::DPadDown, _INPUT_ANY, i),
            _input.getButtonAction(::DPadRight, _INPUT_ANY, i),
            _input.getButtonAction(::DPadLeft, _INPUT_ANY, i),

            _input.getButtonAction(::LeftBumper, _INPUT_ANY, i),
            _input.getButtonAction(::RightBumper, _INPUT_ANY, i),

            _input.getButtonAction(::SelectButton, _INPUT_ANY, i),
            _input.getButtonAction(::StartButton, _INPUT_ANY, i),
            _input.getButtonAction(::GuideButton, _INPUT_ANY, i),

            _input.getButtonAction(::LeftStickButton, _INPUT_ANY, i),
            _input.getButtonAction(::RightStickButton, _INPUT_ANY, i)
        ];
        foreach(c,d in buttonData){
            controllerViews[i].setButtonVisible(c, d);
        }
        //Triggers
        controllerViews[i].setTriggerAxis(true, _input.getTriggerAction(::LeftTrigger, _INPUT_ANY, i));
        controllerViews[i].setTriggerAxis(false, _input.getTriggerAction(::RightTrigger, _INPUT_ANY, i));

        //Sticks
        local leftX = _input.getAxisActionX(::LeftStick, _INPUT_ANY, i);
        local leftY = _input.getAxisActionY(::LeftStick, _INPUT_ANY, i);
        local rightX = _input.getAxisActionX(::RightStick, _INPUT_ANY, i);
        local rightY = _input.getAxisActionY(::RightStick, _INPUT_ANY, i);
        controllerViews[i].setStickPosition(true, leftX, leftY);
        controllerViews[i].setStickPosition(false, rightX, rightY);
    }
}

function end(){
    print("ending");
}
