function dialogString(dialog, actorId){
    ::DialogGui.presentDialog(dialog);
}

function dialogBegan(){
    ::DialogGui.setVisible(true);
}

function dialogEnded(){
    ::DialogGui.setVisible(false);
}
