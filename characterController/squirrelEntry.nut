const TRIGGER = 0;
const DAMAGE = 1;

function giveMoney(amount){
    print("The player gets " + amount + " money.");
    playerMoney += amount;
    PlayerActionManager.updateMoneyLabel();
}

function damageTrigger(id, eventType){
    //Assume to damage the player.
    print("Damage player");
}

function triggerResponse(id, eventType){
    if(eventType == _COLLISION_ENTER){
        ::PlayerActionManager.registerInput( ::dialogScripts[id] );
    }
    else if(eventType == _COLLISION_LEAVE){
        ::PlayerActionManager.removeInput( "" );
    }
}

function worldTrigger(){
    print("Player entered thing");
}

function start(){
    _doFile("res://player.nut");
    _doFile("res://entity.nut");
    _doFile("res://PlayerActionManager.nut");
    _doFile("res://DialogGui.nut");

    _slotManager.setCurrentMap("characterControllerMap");
    _world.createWorld();

    ::dialogScripts <- [
        "res://DialogScripts/SimpleDialog.dialog",
        "res://DialogScripts/OtherDialog.dialog",
    ];
    ::playerMoney <- 0;
    ::DialogGui.initialise();
    ::collisionObjects <- [];
    ::PlayerActionManager.setup();

    ::player <- Player();
    ::player.initialise();

    ::createdEntities <- [];

    ::performedAction <- false;

    {
        //Create some entities.
        local e = Entity(SlotPosition(0, 0, 200, 1, 200), 0);
        local ee = Entity(SlotPosition(0, 0, 250, 1, 250), 1);

        createdEntities.append(e);
        createdEntities.append(ee);
    }

    //Insert triggers for the world.
    {
        local senderTable = {
            "func" : worldTrigger,
            "id" : 1,
            "type" : _COLLISION_PLAYER,
            "event" : _COLLISION_ENTER
        };
        local shape = _physics.getSphereShape(20);

        local collisionObject = _physics.collision[TRIGGER].createSender(senderTable, shape);
        _physics.collision[TRIGGER].addObject(collisionObject);
        collisionObject.setPosition(Vec3(280, 0, 300));

        collisionObjects.append(collisionObject);
    }

    //Insert damage objects to the world
    {
        local senderTable = {
            "func" : damageTrigger,
            "id" : 0,
            "type" : _COLLISION_PLAYER | _COLLISION_ENEMY,
            "event" : _COLLISION_INSIDE
        };
        local shape = _physics.getSphereShape(20);

        local collisionObject = _physics.collision[DAMAGE].createSender(senderTable, shape);
        _physics.collision[DAMAGE].addObject(collisionObject);
        collisionObject.setPosition(Vec3(200, 0, 300));

        collisionObjects.append(collisionObject);
    }

}

function update(){
    ::player.update();
    foreach(i in createdEntities){
        i.update();
    }


    if(_input.getButtonAction(::TalkHandle)){
        if(!performedAction && !_dialogSystem.isDialogExecuting()){
            performedAction = true;
            ::PlayerActionManager.performAction();
        }
        if(!performedAction && _dialogSystem.isDialogBlocked()){
            performedAction = true;
            _dialogSystem.unblock();
        }
    }else{
        performedAction = false;
    }
}

function end(){
    print("ending");
}
