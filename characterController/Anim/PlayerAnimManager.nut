enum PlayerStateTypes{
    PLAYER_STATE_IDLE,
    PLAYER_STATE_RUNNING,
};

::PlayerAnimManager <- class{
    mCharacterSceneNode = null;

    mCharacterMeshItem = null;
    mCharacterMeshSkeleton = null;

    mCurrentAnimation = null;
    mCurrentAnimLifetime = 0.0;

    mPlayerStates = null;
    mCurrentPlayerState = null;
    mPlayerStateData = null;

    constructor(){
        mPlayerStates = ::PlayerAnimManagerStates;
        mPlayerStateData = {
            "manager": this,
            "velocity": null
        };

        mCharacterSceneNode = _scene.getRootSceneNode().createChildSceneNode();
        mCharacterMeshItem = _scene.createItem("baseMesh.mesh");
        mCharacterSceneNode.attachObject(mCharacterMeshItem);

        mCharacterSceneNode.setScale(Vec3(0.5));

        if(mCharacterMeshItem.hasSkeleton()){
            mCharacterMeshSkeleton = mCharacterMeshItem.getSkeleton();
        }

        setCurrentState(PlayerStateTypes.PLAYER_STATE_IDLE);
    }

    function notifyMovement(newVecPos, currentVelocity){
        //Ideally this would be done with a comonent, but I don't have one right now.
        mCharacterSceneNode.setPosition(newVecPos);

        if(
            currentVelocity.x < 0.1 && currentVelocity.z < 0.1 &&
            currentVelocity.x > -0.1 && currentVelocity.z > -0.1
            ) return;



            /* if(mCurrentAnimation == null){

            }
            mCurrentAnimLifetime = 10.0; */
        if(mCurrentPlayerState != null){
            mPlayerStateData.velocity = currentVelocity;
            mCurrentPlayerState.notifyMovement(mPlayerStateData);
        }
    }

    function update(){
        if(mCurrentAnimation){
            //mCurrentAnimation.addTime(0.01);

            /* mCurrentAnimLifetime--;
            if(mCurrentAnimLifetime <= 0.0){
                mCurrentAnimation.setEnabled(false);
                mCurrentAnimation = null;
            } */
        }

        if(mCurrentPlayerState != null){
            mCurrentPlayerState.update(mPlayerStateData);
        }

    }

    function setCurrentAnimation(animName){
        if(!mCharacterMeshSkeleton) return;

        if(mCurrentAnimation){
            mCurrentAnimation.setEnabled(false);
        }

        local anim = mCharacterMeshSkeleton.getAnimation(animName);
        anim.setEnabled(true);
        anim.setLoop(true);
        mCurrentAnimation = anim;
    }

    function setCurrentState(stateId){
        if(mCurrentPlayerState){
            mCurrentPlayerState.end(mPlayerStateData);
        }

        mCurrentPlayerState = mPlayerStates[stateId];
        mCurrentPlayerState.start(mPlayerStateData);
    }
}

::PlayerAnimManagerStates <- [

    { //Idle
        function start(data){
            data.manager.setCurrentAnimation("Idle");
            print("Idle");
        },

        function update(data){
            local anim = data.manager.mCurrentAnimation;

            anim.addTime(0.01);
        },

        function end(data){

        },

        function notifyMovement(data){
            if(abs(data.velocity.x) > 3.0 || abs(data.velocity.z) > 3.0){
                data.manager.setCurrentState(PlayerStateTypes.PLAYER_STATE_RUNNING);
            }
        }
    },

    { //Run
        function start(data){
            data.manager.setCurrentAnimation("Run");
            print("Run");
        },

        function update(data){
            local anim = data.manager.mCurrentAnimation;

            anim.addTime(0.01);
        },

        function end(data){

        },

        function notifyMovement(data){
            if(abs(data.velocity.x) < 1.0 && abs(data.velocity.z) < 1.0){
                data.manager.setCurrentState(PlayerStateTypes.PLAYER_STATE_IDLE);
            }
        }
    }

];
