::Player <- class{

    entity = null;
    body = null;

    mAnimManager = null;

    mInputGiven = false;

    constructor(){
    }

    function initialise(){
        _doFile("res://Anim/PlayerAnimManager.nut");

        mAnimManager = PlayerAnimManager();
        //mPlayerMeshNode = mAnimManager.mCharacterSceneNode;

        entity = _entity.create(SlotPosition(0, 0, 300, 200, 300));
        //_component.mesh.add(entity, "baseMesh.mesh");
        _component.script.add(entity, "res://PlayerEntityScript.nut");

        local shape = _physics.getCapsuleShape(4, 1);
        local constructionInfo = {"mass": 20, "friction": 0.0};
        this.body = _physics.dynamics.createRigidBody(shape, constructionInfo);
        //this.body.setLinearFactor(1, 0, 1);
        _physics.dynamics.addBody(body);
        _component.rigidBody.add(entity, body);


        local receiverInfo = {
            "type" : _COLLISION_PLAYER,
        };
        local shape = _physics.getSphereShape(2);

        local collisionObject = _physics.collision[TRIGGER].createReceiver(receiverInfo, shape);
        _physics.collision[TRIGGER].addObject(collisionObject);

        local damageObject = _physics.collision[DAMAGE].createReceiver(receiverInfo, shape);
        _physics.collision[DAMAGE].addObject(damageObject);

        _component.collision.add(entity, collisionObject, damageObject);

        /* ::otherBody <-_physics.dynamics.createRigidBody(shape, constructionInfo);
        _physics.dynamics.addBody(otherBody);
        ::mesh <- _mesh.create("cube");
        mesh.attachRigidBody(otherBody);
        ::otherBody.setPosition(SlotPosition(0, 0, 245, 3, 250));
        ::otherBody.setLinearVelocity(1, 1, 0);

        this.body.setLinearVelocity(3, 3, 0); */

        entity.setPosition(SlotPosition(0, 0, 250, 4, 250));

        _input.setActionSets({
            "default" : {
                "Buttons" : {
                    "Jump": "#Jump",
                    "Talk": "#Talk"
                },
                "StickPadGyro" : {
                    "Move":"#Move"
                }
            }
        });

        ::JumpHandle <- _input.getButtonActionHandle("Jump");
        ::TalkHandle <- _input.getButtonActionHandle("Talk");
        ::MoveHandle <- _input.getAxisActionHandle("Move");

        _input.mapControllerInput(0, ::MoveHandle);
        _input.mapControllerInput(0, ::JumpHandle);
        _input.mapKeyboardInput(104, ::TalkHandle);

        _input.mapKeyboardInput(122, ::JumpHandle);
        _input.mapKeyboardInputAxis(100, 115, 97, 119, ::MoveHandle);
    }

    function enoughMovement(val){
        return val > 0.1 || val < -0.1;
    }

    function update(){
        applyInputMovement();
        applyJump();
        mAnimManager.update();

        local bodyVel = this.body.getLinearVelocity();

        //Gradually dither the velocity to make it less slippy.
        if(!mInputGiven){
            this.body.setLinearVelocity(bodyVel * Vec3(0.65, 1.0, 0.65));
        }
        mInputGiven = false;
    }

    function applyJump(){
        if(!_input.getButtonAction(::JumpHandle)) return;

        local bodyVel = this.body.getLinearVelocity();
        this.body.setLinearVelocity(bodyVel + Vec3(0, 1, 0));
    }

    function applyInputMovement(){
        if(_dialogSystem.isDialogExecuting()) return;
        local amount = Vec3(_input.getAxisActionX(::MoveHandle) * 7, 0, _input.getAxisActionY(::MoveHandle) * 7);
        if( !enoughMovement(amount.x) && !enoughMovement(amount.z) ) return;

        local bodyVel = this.body.getLinearVelocity();
        local totalVel = amount + bodyVel;
        if(totalVel.x < -20) totalVel.x = -20;
        if(totalVel.z < -20) totalVel.z = -20;
        if(totalVel.x > 20) totalVel.x = 20;
        if(totalVel.z > 20) totalVel.z = 20;
        this.body.setLinearVelocity(totalVel);
        mInputGiven = true;
    }

    function move(pos){
        local realPos = pos.toVector3();

        //_camera.setPosition(realPos.x, realPos.y + 100, realPos.z + 100);
        _camera.setPosition(realPos + Vec3(0, 50, 50));
        //_camera.setPosition(pos);
        _camera.lookAt(realPos.x, realPos.y, realPos.z);
        _world.setPlayerPosition(pos);

        mAnimManager.notifyMovement(realPos, this.body.getLinearVelocity());
    }

};
