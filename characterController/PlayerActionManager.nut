//Manages what inputs the user has access to.

::PlayerActionManager <- {
    availableActions = [],

    firstLabel = null,
    moneyLabel = null,

    function registerInput(action){
        availableActions.append(action);
        updateList();
    }

    function updateMoneyLabel(){
        moneyLabel.setText("money: " + ::playerMoney.tostring());
    }

    function removeInput(action){
        availableActions.pop();
        updateList();
    }

    function updateList(){
        if(availableActions.len() <= 0){
            firstLabel.setText("");
            firstLabel.setHidden(true);
            return
        }
        firstLabel.setText(availableActions[0]);
        firstLabel.setHidden(false);
    }

    function performAction(){
        if(availableActions.len() <= 0) return;

        _dialogSystem.compileAndRunDialog(availableActions[0]);

    }

    function setup(){
        local win = _gui.createWindow();
        win.setPosition(0, 0);
        win.setSize(300, 300);

        local layout = _gui.createLayoutLine();

        moneyLabel = win.createLabel();
        moneyLabel.setText(" ");

        firstLabel = win.createLabel();
        firstLabel.setText(" ");

        layout.addCell(moneyLabel);
        layout.addCell(firstLabel);

        updateMoneyLabel();

        layout.layout();

        updateList();
    }
};
