//This class encapsulates an entity functionality.
//Really they should be based around the entity component system, but this is just to make it easier for now.

::Entity <- class{

    entity = null;
    collisionObject = null;
    currentPos = null;
    count = 0;

    constructor(pos, id){

        entity = _entity.createTracked(pos);
        _component.mesh.add(entity, "cube");

        local shape = _physics.getCapsuleShape(1, 1);
        local ePos = pos.toVector3();
        local body = _physics.dynamics.createRigidBody(shape, {"origin": ePos, "mass":0});
        _physics.dynamics.addBody(body);

        _component.rigidBody.add(entity, body);

        //Collision sender
        local senderTable = {
            "func" : "triggerResponse",
            "path" : "res://squirrelEntry.nut"
            "id" : id,
            "type" : _COLLISION_PLAYER,
            "event" : _COLLISION_ENTER | _COLLISION_LEAVE
        };
        local shape = _physics.getSphereShape(4);

        collisionObject = _physics.collision[TRIGGER].createSender(senderTable, shape);
        _physics.collision[TRIGGER].addObject(collisionObject);
        collisionObject.setPosition(ePos);

        _component.collision.add(entity, collisionObject);

        currentPos = pos;
    }

    function update(){
        count++;
        if(count > 100) count = -100;
        currentPos.move(count > 0 ? 0.1 : -0.1, 0, 0);
        entity.setPosition(currentPos);
    }

};
