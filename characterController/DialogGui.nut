::DialogGui <- {

    dialogWindow = null,
    dialogLabel = null,

    function initialise(){
        dialogWindow = _gui.createWindow();
        dialogWindow.setPosition(500, 300);
        dialogWindow.setSize(900, 250);
        dialogWindow.setHidden(true);

        dialogLabel = dialogWindow.createLabel();
        dialogLabel.setText("");
    }

    function setVisible(visible){
        dialogWindow.setHidden(!visible);
    }

    function presentDialog(text){
        dialogLabel.setText(text);
    }
};
