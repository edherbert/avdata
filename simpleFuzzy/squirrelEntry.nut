function start(){
    _doFile(_settings.getDataDirectory() + "/Fuzzy/FuzzySystem.nut");

    local quality = fuzzy.Variable(fuzzy.operator.arange(0, 11), "quality");
    quality.term("poor", fuzzy.operator.trimf(quality.universe, [0, 0, 5]));
    quality.term("average", fuzzy.operator.trimf(quality.universe, [0, 5, 10]));
    quality.term("good", fuzzy.operator.trimf(quality.universe, [5, 10, 10]));

    local rules = [
        //These are entirely nonsense.
        fuzzy.Rule([quality.getTerm("poor"), quality.getTerm("average")], quality.getTerm("good"))
    ];

    local system = fuzzy.System(rules);
    system.produce();

}

function update(){
}

function end(){
    print("ending");

}
