//My attempt to write a very basic fuzzy logic implementation in squirrel.
//It's largely based off of rlnx/fuzzy-lite

::fuzzy <- {

};

fuzzy.operator <- {};

fuzzy.operator.min <- function(x, y){
    if(x > y) return y;
    return x;
}

fuzzy.operator.max <- function(x, y){
    if(x < y) return y;
    return x;
}

fuzzy.operator.prod <- function(x, y){
    return x * y;
}

fuzzy.operator.sum <- function(x, y){
    return x + y - x * y;
}

fuzzy.operator.centroid <- function(universe, xmf){
    //TODO Figure this out later.
    return 1;
}

fuzzy.operator.interpMembership <- function(x, xmf, xx){
    //TODO will be done later.

    return 1;
}

fuzzy.operator.arange <- function(start, end){
    local retArray = [];
    for(local i = start; i < end; i++){
        retArray.append(i);
    }
    return retArray;
}

fuzzy.operator.trimf <- function(x, abc){
    function compArray(val, arr, compType){
        local retArr = array(arr.len(), false);
        foreach(i, c in arr){
            retArr[c] = compType ? i < val : val < i;
        }
        return retArr;
    }
    function compEqualArray(val, arr){
        local retArr = array(arr.len(), false);
        foreach(i, c in arr){
            retArr[c] = i == val;
        }
        return retArr;
    }
    function logicalAndArray(arr1, arr2){
        local retArr = array(arr1.len(), false);
        //Assumes both arrays have the same size.
        for(local i = 0; i < arr1.len(); i++){
            retArr[i] = arr1[i] && arr2[i];
        }
        return retArr;
    }
    function nonzero(arr){
        local retArray = [];
        foreach(i, c in arr){
            if(c != false) retArray.append(i);
        }
        return retArray;
    }

    local a = abc[0];
    local b = abc[1];
    local c = abc[2];
    local y = array(x.len(), 0);

    if(a != b){
        local arr1 = compArray(a, x, false);
        local arr2 = compArray(b, x, true);

        local idx = nonzero(logicalAndArray(arr1, arr2));

        foreach(i in idx){
            y[i] = (x[i] - a).tofloat() / (b - a).tofloat();
        }
    }
    if(b != c){
        local arr1 = compArray(b, x, false);
        local arr2 = compArray(c, x, true);

        local idx = nonzero(logicalAndArray(arr1, arr2));

        foreach(i in idx){
            y[i] = (c - x[i]).tofloat() / (c - b).tofloat();
        }
    }

    local idx = nonzero(compEqualArray(b, x));
    foreach(i in idx){
        y[i] = 1;
    }

    return y;
}

fuzzy.Term <- class{
    universe = null;
    xmf = null;
    variable = null;

    constructor(universe, xmf){
        this.universe = universe;
        this.xmf = xmf;
    }

    function membershipValue(value){
        return fuzzy.operator.interpMembership(this.universe, this.xmf, value);
    }

    function varname(){
        //The name of the variable it's assigned to.
        return variable.name;
    }
};

fuzzy.Variable <- class{
    universe = null;
    name = null;
    _terms = null;

    constructor(universe, name){
        this.universe = universe;
        this.name = name;
        this._terms = { };
    }

    function term(name, val){
        local t = fuzzy.Term(this.universe, val);
        t.variable = this;
        this._terms[name] <- t;
    }

    function getTerm(name){
        return this._terms[name];
    }
};

fuzzy.Rule <- class{
    antecedentsTable = null;

    constructor(antecedents, consequent){
        _setupAntecedentsTable(antecedents);
    }

    function _setupAntecedentsTable(a){
        antecedentsTable = {};
        foreach(t in a){
            antecedentsTable[t.varname()] <- t;
        }
    }

    function getAntecedentTermByVarName(name){
        if(name in antecedentsTable){
            return antecedentsTable[name];
        }
        return null;
    }
};

fuzzy.System <- class{
    rules = null;
    values = null;

    agregation_operator = null;
    activation_operator = null;
    accumulation_operator = null;
    defuzzification_operator = null;

    constructor(rules){
        this.rules = rules;
        this.values = {"quality": 6.5, "service": 9.8};

        this.agregation_operator = fuzzy.operator.min;
        this.activation_operator = fuzzy.operator.min;
        this.accumulation_operator = fuzzy.operator.max;
        this.defuzzification_operator = fuzzy.operator.centroid;
    }

    function produce(){
        print("Doing fuzzy stuff.");

        foreach(r in this.rules){
            local result = _fuzzification(r, this.values);
            foreach(i in result[0]) print(i);
            foreach(i in result[1]) print(i);
        }
    }

    function _fuzzification(r, v){
        local result = [];
        local varnames = [];
        foreach(key, value in v){
            local t = r.getAntecedentTermByVarName(key);
            if(t != null){
                local memValue = t.membershipValue(value);

                result.append(memValue);
                varnames.append(key);
            }
        }
        return [result, varnames];
    }
};
