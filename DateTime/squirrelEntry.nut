function setupGui(){
    ::win <- _gui.createWindow();
    ::win.setPosition(0, 0);
    ::win.setSize(500, 500);

    local layout = _gui.createLayoutLine();

    local labels = ["Get date for epoch", "Get date", "Get date UTC"];
    local callbacks = [
        function(widget, action){
            updateDateString(0);
        },
        function(widget, action){
            updateDateString(_system.time());
        },
        function(widget, action){
            updateDateString(_system.time(), _TIME_FORMAT_UTC);
        }
    ];
    foreach(i,c in labels){
        local button = ::win.createButton();
        button.setText(c);
        button.attachListenerForEvent(callbacks[i], 2);
        button.setUserId(i);
        layout.addCell(button);
    }

    ::dateLabel <- ::win.createLabel();
    local sysTime = _system.time();
    print(sysTime);
    updateDateString(sysTime);
    layout.addCell(::dateLabel);

    layout.layout();
}

::updateDateString <- function(time, timeFormat = _TIME_FORMAT_LOCAL){
    local d = _system.date(time, timeFormat);
    local labelString = "";
    foreach(i,c in d){
        labelString += format("%s: %i\n", i, c);
    }
    ::dateLabel.setText(labelString);
}

function start(){
    print("starting");

    setupGui();
}

function update(){

}

function end(){
    print("ending");
}
