function callbackFunction(){
    print("Collision");
}

function start(){
    print("starting");

    //The world needs to be created for shapes to be created.
    _world.createWorld();
    ::count <- 0;
    _camera.setPosition(0, 0, 40);
    _camera.lookAt(0, 0, 0);

    local cube = _physics.getCubeShape(1, 1, 1);
    local sphere = _physics.getSphereShape(1);

    local senderInfo = {
        "func" : callbackFunction,
        "id" : 1,
        "type" : _COLLISION_PLAYER,
        "event" : _COLLISION_INSIDE | _COLLISION_ENTER | _COLLISION_LEAVE
    };
    local receiverInfo = {
        "type" : _COLLISION_PLAYER,
    };

    ::sender <- _physics.collision[0].createSender(senderInfo, cube);
    ::receiver <- _physics.collision[0].createReceiver(receiverInfo, sphere);

    _physics.collision[0].addObject(sender);
    _physics.collision[0].addObject(receiver);
}

function update(){
    count += 0.01;

    local pos = sin(count);
    ::sender.setPosition(pos * 10, 0, 0);
    ::receiver.setPosition(-pos * 10, 0, 0);
}

function end(){
    print("ending");
}
