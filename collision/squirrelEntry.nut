::PointEntry <- class{
    mPoint = null;
    mPosition = null;
    mVelocity = null;
    mTexture = null;
    mCircle = null;
    mRadius = null;

    constructor(circle, point, position, velocity, texture, radius){
        mCircle = circle;
        mPoint = point;
        mPosition = position;
        mVelocity = velocity;
        mTexture = texture;
        mRadius = radius;
    }

    function setPosition(world, pos){
        world.setPositionForPoint(mPoint, pos.x, pos.y);
        if(mCircle){
            mTexture.setPosition(pos - mRadius);
        }else{
            mTexture.setPosition(pos);
        }
    }

    function setTriggerTextureChange(change){
        if(change){
            mTexture.setColour(1, 0, 0, 1);
        }else{
            mTexture.setColour(1, 1, 1, 1);
        }
    }

};

function start(){

    ::mCurrentWorld <- CollisionWorld(_COLLISION_WORLD_BRUTE_FORCE);
    ::mActivePoints <- {};

    const WIDTH = 1000;
    const HEIGHT = 1000;
    local levelSize = Vec2(WIDTH, HEIGHT);

    for(local i = 0; i < 40; i++){
        local entry = createEntry_(levelSize, _random.randInt(2) == 1);
    }

    ::MouseEntry <- createEntry_(levelSize, true);
}

function createEntry_(levelSize, circle){
    local pos = _random.randVec2() * levelSize;
    local radius = 20 + _random.randInt(0, 5);
    local addedPoint = null;
    local tex = null;
    if(!circle){
        radius *= 2;
        addedPoint = mCurrentWorld.addCollisionRectangle(pos.x, pos.y, radius, radius);

        tex = MovableTexture("cat1.jpg");
        tex.setPosition(pos);
        tex.setSize(radius, radius);
        tex.setColour(1, 1, 1, 1);
    }else{
        addedPoint = mCurrentWorld.addCollisionPoint(pos.x, pos.y, radius);

        tex = MovableTexture("circle.png");
        tex.setPosition(pos - radius);
        tex.setSize(radius * 2, radius * 2);
        tex.setColour(1, 1, 1, 1);
    }
    local entry = PointEntry(circle, addedPoint, pos, _random.randVec2(), tex, radius);

    ::mActivePoints.rawset(addedPoint, entry);

    return entry;
}

function update(){
    //Move the points
    foreach(i in mActivePoints){
        i.mPosition += i.mVelocity;
        if(
            i.mPosition.x >= WIDTH ||
            i.mPosition.y >= HEIGHT ||
            i.mPosition.x < 0.0 ||
            i.mPosition.y < 0.0
        )
        {
            i.mVelocity = -(i.mVelocity);
        }
        i.setPosition(mCurrentWorld, i.mPosition);
    }

    //Check for collisions
    mCurrentWorld.processCollision();
    print(mCurrentWorld.getNumCollisions());
    for(local i = 0; i < mCurrentWorld.getNumCollisions(); i++){
        local collisionStatus = (mCurrentWorld.getCollisionPairForIdx(i) & 0xF000000000000000) >> 60;
        local pair = mCurrentWorld.getCollisionPairForIdx(i) & 0xFFFFFFFFFFFFF;

        local first = (pair&0xFFFFFFF);
        local second = (pair >> 30) & 0xFFFFFFF;
        mActivePoints[first].setTriggerTextureChange(true);
        mActivePoints[second].setTriggerTextureChange(true);
    }

    ::MouseEntry.setPosition(mCurrentWorld, Vec2(_input.getMouseX(), _input.getMouseY()));
}

function end(){
}

function widgetCallback(widget, action){

}

function setupGui(){
    ::win <- _gui.createWindow();
    ::win.setPosition(0, 0);
    ::win.setSize(500, 500);

    local layout = _gui.createLayoutLine();
    local labels = ["Brute Force"];
    foreach(i,c in labels){
        local button = ::win.createButton();
        button.setText(c);
        button.attachListener(widgetCallback);
        button.setUserId(i);
        layout.addCell(button);
    }
    layout.layout();
}