//An example showing the usage of the dialog system.

function start(){
    _doFile(_settings.getDataDirectory() + "/../common/jankyGui/jankyText.nut");

    _dialogSystem.compileAndRunDialog(_settings.getDataDirectory() + "/../common/dialog/SimpleDialog.dialog");

    ::keyPressed <- false;

    ::currentDialog <- janky.text("");
    currentDialog.setPosition(10, 10);
    currentDialog.setSize(16);
    currentDialog.setLayer(1);

    ::acceptActionHandle <- _input.getButtonActionHandle("Accept");
}

::updateText <- function(str){
    currentDialog.setText(str);
}

function update(){
    local systemKeyPress = _input.getButtonAction(::acceptActionHandle);
    if(!systemKeyPress) keyPressed = false;

    if(systemKeyPress && !keyPressed){
        _dialogSystem.unblock();

        keyPressed = true;
    }
}

function end(){

}
