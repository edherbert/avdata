function firstFunction(){
    print("This is the first function!");
}

function dialogString(dialog, actorId){
    print(dialog);
    updateText(dialog);
}

function dialogEnded(){
    updateText("");
}

function dialogBegan(){
    print("Dialog began");
}
